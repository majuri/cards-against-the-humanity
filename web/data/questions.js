var questions = [
  {
    id: 0,
    text: "TSA guidelines now prohibit __________ on airplanes.",
    requiredAnswers: 1
  }
  ,{
    id: 1,
    text: "It's a pity that kids these days are all getting involved with __________.",
    requiredAnswers: 1
  }
  ,{
    id: 2,
    text: "In 1,000 years, when paper money is but a distant memory, __________ will be our currency.",
    requiredAnswers: 1
  }
  ,{
    id: 3,
    text: "Major League Baseball has banned __________ for giving players an unfair advantage.",
    requiredAnswers: 1
  }
  ,{
    id: 4,
    text: "What is Batman's guilty pleasure?",
    requiredAnswers: 1
  }
  ,{
    id: 5,
    text: "Next from J.K. Rowling: Harry Potter and the Chamber of __________.",
    requiredAnswers: 1
  }
  ,{
    id: 6,
    text: "I'm sorry, Professor, but I couldn't complete my homework because of __________.",
    requiredAnswers: 1
  }
  ,{
    id: 7,
    text: "What did I bring back from Mexico?",
    requiredAnswers: 1
  }
  ,{
    id: 8,
    text: "__________? There's an app for that.",
    requiredAnswers: 1
  }
  ,{
    id: 9,
    text: "__________. Betcha can't have just one!",
    requiredAnswers: 1
  }
  ,{
    id: 10,
    text: "What's my anti-drug?",
    requiredAnswers: 1
  }
  ,{
    id: 11,
    text: "While the United States raced the Soviet Union to the moon, the Mexican government funneled millions of pesos into research on __________.",
    requiredAnswers: 1
  }
  ,{
    id: 12,
    text: "In the new Disney Channel Original Movie, Hannah Montana struggles with __________ for the first time. ",
    requiredAnswers: 1
  }
  ,{
    id: 13,
    text: "What's my secret power?",
    requiredAnswers: 1
  }
  ,{
    id: 14,
    text: "What's the new fad diet?",
    requiredAnswers: 1
  }
  ,{
    id: 15,
    text: "What did Vin Diesel eat for dinner?",
    requiredAnswers: 1
  }
  ,{
    id: 16,
    text: "When Pharaoh remained unmoved, Moses called down a Plague of __________.",
    requiredAnswers: 1
  }
  ,{
    id: 17,
    text: "How am I maintaining my relationship status?",
    requiredAnswers: 1
  }
  ,{
    id: 18,
    text: "What's the crustiest?",
    requiredAnswers: 1
  }
  ,{
    id: 19,
    text: "In L.A. County Jail, word is you can trade 200 cigarettes for __________.",
    requiredAnswers: 1
  }
  ,{
    id: 20,
    text: "After the earthquake, Sean Penn brought __________ to the people of Haiti.",
    requiredAnswers: 1
  }
  ,{
    id: 21,
    text: "Instead of coal, Santa now gives the bad children __________.",
    requiredAnswers: 1
  }
  ,{
    id: 22,
    text: "Life for American Indians was forever changed when the White Man introduced them to __________.",
    requiredAnswers: 1
  }
  ,{
    id: 23,
    text: "What's Teach for America using to inspire inner city students to succeed?",
    requiredAnswers: 1
  }
  ,{
    id: 24,
    text: "Maybe she's born with it. Maybe it's __________.",
    requiredAnswers: 1
  }
  ,{
    id: 25,
    text: "In Michael Jackson's final moments, he thought about __________.",
    requiredAnswers: 1
  }
  ,{
    id: 26,
    text: "White people like __________.",
    requiredAnswers: 1
  }
  ,{
    id: 27,
    text: "Why do I hurt all over?",
    requiredAnswers: 1
  }
  ,{
    id: 28,
    text: "A romantic, candlelit dinner would be incomplete without __________.",
    requiredAnswers: 1
  }
  ,{
    id: 29,
    text: "What will I bring back in time to convince people that I am a powerful wizard?",
    requiredAnswers: 1
  }
  ,{
    id: 30,
    text: "BILLY MAYS HERE FOR __________.",
    requiredAnswers: 1
  }
  ,{
    id: 31,
    text: "The class field trip was completely ruined by __________.",
    requiredAnswers: 1
  }
  ,{
    id: 32,
    text: "What's a girl's best friend?",
    requiredAnswers: 1
  }
  ,{
    id: 33,
    text: "Dear Abby, I'm having some trouble with __________ and would like your advice.",
    requiredAnswers: 1
  }
  ,{
    id: 34,
    text: "When I am President of the United States, I will create the Department of __________.",
    requiredAnswers: 1
  }
  ,{
    id: 35,
    text: "What are my parents hiding from me?",
    requiredAnswers: 1
  }
  ,{
    id: 36,
    text: "What never fails to liven up the party?",
    requiredAnswers: 1
  }
  ,{
    id: 37,
    text: "What gets better with age?",
    requiredAnswers: 1
  }
  ,{
    id: 38,
    text: "__________: Good to the last drop.",
    requiredAnswers: 1
  }
  ,{
    id: 39,
    text: "I got 99 problems but __________ ain't one.",
    requiredAnswers: 1
  }
  ,{
    id: 40,
    text: "__________. It's a trap!",
    requiredAnswers: 1
  }
  ,{
    id: 41,
    text: "MTV's new reality show features eight washed-up celebrities living with __________.",
    requiredAnswers: 1
  }
  ,{
    id: 42,
    text: "What would grandma find disturbing, yet oddly charming?",
    requiredAnswers: 1
  }
  ,{
    id: 43,
    text: "What's the most emo?",
    requiredAnswers: 1
  }
  ,{
    id: 44,
    text: "During sex, I like to think about __________.",
    requiredAnswers: 1
  }
  ,{
    id: 45,
    text: "What ended my last relationship?",
    requiredAnswers: 1
  }
  ,{
    id: 46,
    text: "What's that sound?",
    requiredAnswers: 1
  }
  ,{
    id: 47,
    text: "__________. That's how I want to die.",
    requiredAnswers: 1
  }
  ,{
    id: 48,
    text: "Why am I sticky?",
    requiredAnswers: 1
  }
  ,{
    id: 49,
    text: "What's the next Happy Meal toy?",
    requiredAnswers: 1
  }
  ,{
    id: 50,
    text: "What's there a ton of in heaven?",
    requiredAnswers: 1
  }
  ,{
    id: 51,
    text: "I do not know with what weapons World War III will be fought, but World War IV will be fought with __________.",
    requiredAnswers: 1
  }
  ,{
    id: 52,
    text: "What will always get you laid?",
    requiredAnswers: 1
  }
  ,{
    id: 53,
    text: "__________: Kid-tested, mother-approved.",
    requiredAnswers: 1
  }
  ,{
    id: 54,
    text: "Why can't I sleep at night?",
    requiredAnswers: 1
  }
  ,{
    id: 55,
    text: "What's that smell?",
    requiredAnswers: 1
  }
  ,{
    id: 56,
    text: "What helps Obama unwind?",
    requiredAnswers: 1
  }
  ,{
    id: 57,
    text: "This is the way the world ends / This is the way the world ends / Not with a bang but with __________.",
    requiredAnswers: 1
  }
  ,{
    id: 58,
    text: "Coming to Broadway this season, __________: The Musical.",
    requiredAnswers: 1
  }
  ,{
    id: 59,
    text: "Anthropologists have recently discovered a primitive tribe that worships __________.",
    requiredAnswers: 1
  }
  ,{
    id: 60,
    text: "But before I kill you, Mr. Bond, I must show you __________.",
    requiredAnswers: 1
  }
  ,{
    id: 61,
    text: "Studies show that lab rats navigate mazes 50% faster after being exposed to __________.",
    requiredAnswers: 1
  }
  ,{
    id: 62,
    text: "Next on ESPN2: The World Series of __________.",
    requiredAnswers: 1
  }
  ,{
    id: 63,
    text: "When I am a billionaire, I shall erect a 50-foot statue to commemorate __________.",
    requiredAnswers: 1
  }
  ,{
    id: 64,
    text: "In an attempt to reach a wider audience, the Smithsonian Museum of Natural History has opened an interactive exhibit on __________.",
    requiredAnswers: 1
  }
  ,{
    id: 65,
    text: "War! What is it good for?",
    requiredAnswers: 1
  }
  ,{
    id: 66,
    text: "What gives me uncontrollable gas?",
    requiredAnswers: 1
  }
  ,{
    id: 67,
    text: "What do old people smell like?",
    requiredAnswers: 1
  }
  ,{
    id: 68,
    text: "What am I giving up for Lent?",
    requiredAnswers: 1
  }
  ,{
    id: 69,
    text: "Alternative medicine is now embracing the curative powers of __________.",
    requiredAnswers: 1
  }
  ,{
    id: 70,
    text: "What did the US airdrop to the children of Afghanistan?",
    requiredAnswers: 1
  }
  ,{
    id: 71,
    text: "What does Dick Cheney prefer?",
    requiredAnswers: 1
  }
  ,{
    id: 72,
    text: "During Picasso's often-overlooked Brown Period, he produced hundreds of paintings of __________.",
    requiredAnswers: 1
  }
  ,{
    id: 73,
    text: "What don't you want to find in your Chinese food?",
    requiredAnswers: 1
  }
  ,{
    id: 74,
    text: "I drink to forget __________.",
    requiredAnswers: 1
  }
  ,{
    id: 75,
    text: "__________. High five, bro.",
    requiredAnswers: 1
  }
  ,{
    id: 76,
    text: "He who controls __________ controls the world.",
    requiredAnswers: 1
  }
  ,{
    id: 77,
    text: "The CIA now interrogates enemy agents by repeatedly subjecting them to __________.",
    requiredAnswers: 1
  }
  ,{
    id: 78,
    text: "In Rome, there are whisperings that the Vatican has a secret room devoted to __________.",
    requiredAnswers: 1
  }
  ,{
    id: 79,
    text: "Science will never explain the origin of __________.",
    requiredAnswers: 1
  }
  ,{
    id: 80,
    text: "When all else fails, I can always masturbate to __________.",
    requiredAnswers: 1
  }
  ,{
    id: 81,
    text: "I learned the hard way that you can't cheer up a grieving friend with __________.",
    requiredAnswers: 1
  }
  ,{
    id: 82,
    text: "In its new tourism campaign, Detroit proudly proclaims that it has finally eliminated __________.",
    requiredAnswers: 1
  }
  ,{
    id: 83,
    text: "The socialist governments of Scandinavia have declared that access to __________ is a basic human right.",
    requiredAnswers: 1
  }
  ,{
    id: 84,
    text: "In his new self-produced album, Kanye West raps over the sounds of __________.",
    requiredAnswers: 1
  }
  ,{
    id: 85,
    text: "What's the gift that keeps on giving?",
    requiredAnswers: 1
  }
  ,{
    id: 86,
    text: "This season on Man vs. Wild, Bear Grylls must survive in the depths of the Amazon with only __________ and his wits. ",
    requiredAnswers: 1
  }
  ,{
    id: 87,
    text: "When I pooped, what came out of my butt?",
    requiredAnswers: 1
  }
  ,{
    id: 88,
    text: "In the distant future, historians will agree that __________ marked the beginning of America's decline.",
    requiredAnswers: 1
  }
  ,{
    id: 89,
    text: "What has been making life difficult at the nudist colony?",
    requiredAnswers: 1
  }
  ,{
    id: 90,
    text: "And I would have gotten away with it, too, if it hadn't been for __________.",
    requiredAnswers: 1
  }
  ,{
    id: 91,
    text: "What brought the orgy to a grinding halt?",
    requiredAnswers: 1
  }
  ,{
  id: 92,
  text: "That's right, I killed __________. How, you ask? __________.",
  requiredAnswers: 2
  }
  ,{
    id: 93,
    text: "And the Academy Award for __________ goes to __________.",
    requiredAnswers: 2
  }
  ,{
    id: 94,
    text: "For my next trick, I will pull __________ out of __________.",
    requiredAnswers: 2
  }
  ,{
    id: 95,
    text: "In his new summer comedy, Rob Schneider is __________ trapped in the body of __________.",
    requiredAnswers: 2
  }
  ,{
    id: 96,
    text: "When I was tripping on acid, __________ turned into __________.",
    requiredAnswers: 2
  }
  ,{
    id: 97,
    text: "__________ is a slippery slope that leads to __________.",
    requiredAnswers: 2
  }
  ,{
    id: 98,
    text: "In a world ravaged by __________, our only solace is __________.",
    requiredAnswers: 2
  }
  ,{
    id: 99,
    text: "In M. Night Shyamalan's new movie, Bruce Willis discovers that __________ had really been __________ all along.",
    requiredAnswers: 2
  }
  ,{
    id: 100,
    text: "I never truly understood __________ until I encountered __________.",
    requiredAnswers: 2
  }
  ,{
    id: 101,
    text: "Rumor has it that Vladimir Putin's favorite dish is __________ stuffed with __________.",
    requiredAnswers: 2
  }
  ,{
    id: 102,
    text: "LifetimeÂ® presents __________, the story of __________.",
    requiredAnswers: 2
  }
  ,{
    id: 103,
    text: "What's the next superhero/sidekick duo?",
    requiredAnswers: 2
  }
]
