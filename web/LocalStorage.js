function LocalStorage() {
  this.getCards = function() {
    var stored = localStorage.getItem('answerCard');
    return stored ==! null ? JSON.parse(stored) : [];
  }
  this.setCards = function(cards) {
    localStorage.setItem('answerCard', cards);
  }
}
