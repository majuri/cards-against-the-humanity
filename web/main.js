'use strict';
function AnswerCardView(card, model) {
  var template = $('#answerCardTemplate').html();
  Mustache.parse(template);
  var $element = $(Mustache.render(template, card));
  var $selectButton = $element.find('.answer-button');

  var selected =
    $selectButton
      .asEventStream('click')
      .doAction('.preventDefault');

  var cardProperty =
    selected
      .map(function(selected) {
        return {id: selected.currentTarget.id};
      })
      .toProperty();

  var changes = cardProperty.changes();

  return {
    $selectButton: $selectButton,
    element: $element,
    changes: changes
  }
}

function HandView($element, model) {
  function render(cards) {
    $element.children().remove();
    _.each(cards, addCard);
  }

  function addCard(card, model) {
    var view = new AnswerCardView(card, model);
    $element.append(view.element);
    model.cardSelected.plug(view.changes);
  }

  function selectCard(card) {
    var $selectedCard = $element.find('#answer-' + card.id);
    $selectedCard.toggleClass('selected');
  }

  model.cardAdded
    .onValue(function(card) {
      addCard(card, model);
  });

  model.cardSelected
    .onValue(function(card) {
      selectCard(card);
  });
}

function QuestionView($element, model) {
  function render(question) {
    $element.children().remove();
    addQuestion(question);
  }

  function addQuestion(question) {
    var template = $('#questionCardTemplate').html();
    Mustache.parse(template);
    var element = Mustache.render(template, question);
    $('#question').html(element);
  }

  model.questionAdded
    .onValue(function(question){
      addQuestion(question);
  });
}

function QuestionModel(storage) {
  function addQuestion(newQuestion) {
    return function(q) {
      return q.concat([newQuestion]);
    };
  }

  var questionAdded = new Bacon.Bus();

  questionAdded
    .onValue(addQuestion);

  return {
    questionAdded: questionAdded
  }
};

function HandModel() {
  function filterCards(f) {
    return function(cards) {
      return _.filter(cards, f);
    };
  }

  function mapCards(f) {
    return function(cards) {
      return _.map(cards, f);
    }
  }

  function addCard(newCard) {
    return function(cards) {
      return cards.concat([newCard]);
    };
  }

  function removeCard(deletedCard) {
    return filterCards(function(card) {
      card.id ==! deletedCard.id;
    });
  }

  function setInitialHand(initialHand) {
    cardAdded.plug(initialHand);
  }

  function selectCard(selectedCard) {
    return mapCards(function(card) {
      console.log(selectedCard.id, card.id);
      card.id === selectedCard.id ? selectedCard : card;
    });
  }

  var storage = new LocalStorage();

  var cardAdded = new Bacon.Bus();
  var cardRemoved = new Bacon.Bus();
  var cardSelected = new Bacon.Bus();

  var modifications =
    cardAdded
      .map(addCard)
      .merge(cardRemoved.map(removeCard))
      .merge(cardSelected.map(selectCard));

  var handProperty = modifications.scan([], function(cards, modification) {
    console.log('handProperty',cards, modification);
    return modification(cards);
  });


  return {
    setInitialHand: setInitialHand,
    cardAdded: cardAdded,
    cardRemoved: cardRemoved,
    cardSelected: cardSelected,
    handProperty: handProperty
  }
}

function App() {
  var rules = {
    cardAmount: 7
  };

  var allAnswers = Bacon.fromArray(answers);

  var q = new QuestionModel();
  var hand = new HandModel();
  var question = Bacon.once(_.sample(questions));

  function dealInitialHand(cards, rules) {
    var pickedCards = _.sample(cards, rules.cardAmount);
    return Bacon.fromArray(pickedCards);
  }

  function init() {
    var initialHand = dealInitialHand(answers, rules)
    new HandView($('#hand'), hand);
    hand.setInitialHand(initialHand);
    new QuestionView($('#question'), q);
    q.questionAdded.plug(question);
  };

  return {
    init: init,
    //getQuestion: getQuestion,
    hand: hand
  }
}
var app = new App();
app.init();
